//  node_modules for gulp
const { task, parallel, src, dest, watch, series } = require('gulp'),
      sourcemaps = require('gulp-sourcemaps');

//  node_modules for styles
const sass = require('gulp-sass'),
      cssnano = require('cssnano'),
      postcss = require('gulp-postcss'),
      autoprefixer = require('autoprefixer');
      

//  node_modules for js
const babel = require('gulp-babel'),
      uglify = require('gulp-uglify-es').default;

//  node_modules for html
const htmlmin = require('gulp-htmlmin');

//  node_modules for images
const imagemin = require('gulp-imagemin');

//  node_modules for autoupdates
const browserSync = require('browser-sync').create();

const buildJs = () => {
  return (
    src('src/js/**/*.js')
      .pipe(sourcemaps.init())
      .pipe(babel({
        presets: ['@babel/preset-env']
      }))
      .pipe(uglify())
      .pipe(sourcemaps.write())
      .pipe(dest('build/js'))
  );
};

const buildHtml = () => {
  return (
    src('src/*.html')
      .pipe(htmlmin({
        collapsWhitespace: true,
        preserveLineBreaks: true
      }))
      .pipe(dest('build/html'))
  );
};

const buildImg = () => {
  return (
    //  One '*', without specifying the file format, copies the folder to the build. Therefore '*.*'
    src('src/img/**/*.*') 
      .pipe(imagemin())
      .pipe(dest('build/img'))
  );
};

const buildFonts = () => {
  return (
    //  One '*', without specifying the file format, copies the folder to the build. Therefore '*.*'
    src('src/fonts/**/*.*')
      .pipe(dest('build/fonts'))
  );
};

const buildStyles = () => {
  return (
    src('src/styles/main.scss')
      .pipe(sass()).on('error', sass.logError)
      .pipe(postcss([
        autoprefixer(), cssnano()
      ]))
      .pipe(dest('build/styles'))
  );
};

const sassToCss = () => {
  return (
  src('src/styles/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass()).on('error', sass.logError)
    .pipe(sourcemaps.write())
    .pipe(dest('src/styles'))
    .pipe(browserSync.reload({stream: true}))
  );
};


//  Setups for tasks
const directories = ['src/js/**/*.js', 'src/img/**/*.*', 'src/*.html', 'src/fonts/**/*.*'];

//  Second directory cuz we do another actions for this files
const styleDirectory = ['src/styles/**/*.scss'];

const build = parallel(buildJs, buildImg, buildHtml, buildFonts, buildStyles);

task('build', (cb) => {
  build();

  cb();
});

task('watch', (cb) => {
  //  Server init
  browserSync.init({
    server: {
      baseDir: 'src'
    }
  });

  //  Files tracker
  watch(directories).on('change', browserSync.reload);
  watch(styleDirectory, sassToCss, browserSync.reload());

  cb();
});

exports.default = series('watch');